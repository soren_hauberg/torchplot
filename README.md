# torchplot #

A simple python module for simplifying plotting when using `torch + pyplot`.

### Example usage ###
If you use `pyplot` to show the result of your `torch` model, then you will
quickly find yourself writing code like

	import matplotlib.pyplot as plt
	plt.plot(x.detach().cpu().numpy(), y.detach().cpu().numpy(), color="green")

which quickly get tedious. The `torchplot` module simplify the above to

	import torchplot as plt
	plt.plot(x, y, color="green")

The `torchplot` code works by duplicating all members of `pyplot` such that functions
first convert any input of type `torch.Tensor` to a `numpy.Array` before calling the
corresponding `pyplot` function.

### TODO ###

* Finding smarter ways to get the same syntax (e.g. avoid the use of `exec` within `torchplot`)
* Finding and fixing bugs
* Simplify installation (e.g. support pip)

